package com.example.agendalistview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<Contacto> contactos = new ArrayList<>();
    private EditText edtNombre;
    private EditText edtTelefono;
    private EditText edtTelefono2;
    private EditText edtDireccion;
    private EditText edtNotas;
    private CheckBox cbxFavorito;
    private Contacto saveContact;
    private int savedIndex;
    private int indexActual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombre = (EditText) findViewById(R.id.edtNombre);
        edtTelefono = (EditText) findViewById(R.id.edtTel1);
        edtTelefono2 = (EditText) findViewById(R.id.edtTel2);
        edtDireccion = (EditText) findViewById(R.id.edtDomicilio);
        edtNotas = (EditText) findViewById(R.id.edtNota);
        cbxFavorito = (CheckBox) findViewById(R.id.chkFavorito);
        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);
        Button btnCerrar = (Button) findViewById(R.id.btnCerrar);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtNombre.getText().toString().matches("") || edtTelefono.getText().toString().matches("") ||
                edtTelefono2.getText().toString().matches("") || edtDireccion.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this, "Ingrese todos los datos", Toast.LENGTH_SHORT).show();
                } else {
                    Contacto nContacto = new Contacto();

                    nContacto.setNombre(edtNombre.getText().toString());
                    nContacto.setDomicilio(edtDireccion.getText().toString());
                    nContacto.setNotas(edtNotas.getText().toString());
                    nContacto.setTelefono1(edtTelefono.getText().toString());
                    nContacto.setTelefono2(edtTelefono2.getText().toString());
                    nContacto.setFavorito(cbxFavorito.isChecked());
                   // nContacto.setID(savedIndex);
                   // contactos.add(savedIndex, saveContact);

                    if (saveContact != null) {
                        //contactos.remove(savedIndex);
                        nContacto.setID(savedIndex);
                        for(int y = 0; y < contactos.size(); y++) {
                            if((int) contactos.get(y).getID() == savedIndex) {
                                contactos.set(y,nContacto);
                            }
                        }
                        saveContact = null;
                    } else {
                        int index = indexActual;
                        nContacto.setID(index);
                        contactos.add(nContacto);
                        indexActual++;
                    }
                    Toast.makeText(MainActivity.this, R.string.mensaje, Toast.LENGTH_SHORT).show();
                    limpiar();
                }
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListActivity.class);
                Bundle bObject = new Bundle();
                bObject.putSerializable("contactos", contactos);
                bObject.putInt("indexActual", indexActual);
                i.putExtras(bObject);
                startActivityForResult(i, 0);
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(intent != null) {
            Bundle oBundle = intent.getExtras();
            if(oBundle.getInt("indexActual") > 0) {
                indexActual = oBundle.getInt("indexActual");
            }
            if(oBundle.getSerializable("contactos") instanceof Contacto) {
                saveContact = (Contacto) oBundle.getSerializable("contactos");
                savedIndex = oBundle.getInt("index");
                edtNombre.setText(saveContact.getNombre());
                edtDireccion.setText(saveContact.getDomicilio());
                edtNotas.setText(saveContact.getNotas());
                edtTelefono.setText(saveContact.getTelefono1());
                edtTelefono2.setText(saveContact.getTelefono2());
                cbxFavorito.setChecked(saveContact.isFavorito());
            }
            contactos = (ArrayList<Contacto>) oBundle.getSerializable("contactosSaved");
            boolean nuevo = oBundle.getBoolean("nuevo");
            if(nuevo){
                limpiar();
            }
        }
    }


    public void limpiar() {
        edtNombre.setText("");
        edtDireccion.setText("");
        edtTelefono.setText("");
        edtTelefono2.setText("");
        edtNotas.setText("");
        cbxFavorito.setChecked(false);
        saveContact = null;
    }
}
